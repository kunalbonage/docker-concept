import '../css/main.css';
import '../css/input-elements.css';
import ReactDOM from 'react-dom';
import React, { Component } from 'react';

class ReactTest extends Component {
    render() {
        console.log('test component');
        return (
            <div>
                Hello
        </div>
        )
    }
}

ReactDOM.render(
    <ReactTest />,
    document.getElementById("root")
);